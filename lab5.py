# Добавить в программу, разработанную на практическом занятии №3,
#  дополнительную процедуру, обеспечивающую решение требуемой задачи.
#  Дополнительная процедура (метод) должна выполнять следующие функции:
# Сортировка элементов контейнера по заданному ключу.
# Ключ – Значение, вычисленное процедурой, введенной в лабораторной работе №3.
# Сравнение ключей двух программных объектов должно быть оформлено в виде вспомогательной процедуры,
# проверяющей, какой из ключей меньше (больше…).
# Процедура должна использовать в качестве параметров два обобщения,
# определяя внутри истинную структуру сравниваемых артефактов с использованием механизмов,
# присущих применяемым парадигмам программирования.
import json
import math
import sys
from enum import Enum


def parse_file(filename):
    try:
        with open(filename) as f:
            data = json.load(f)
        return data
    except IOError:
        print(f"Невозможно открыть файл {filename}")
        exit(-1)
    except ValueError:
        print(f"Формат данных в файле {filename} не является валидным JSON.")
        exit(-1)


def format_coordinates(coordinates):
    return f"({coordinates['x']},{coordinates['y']})"


def distance_between_point(x1, y1, x2, y2):
    return math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2))


def calculate_perimeter(shapes, shape):
    coordinates = shape['coordinates_raw']
    if shape['type'] == shapes['rectangle'].name:
        width = distance_between_point(float(coordinates[0]['x']), 0, float(coordinates[1]['x']), 0)
        height = distance_between_point(float(coordinates[0]['y']), 0, float(coordinates[1]['y']), 0)
        perimeter = (width + height) * 2
    else:
        if shape['type'] == shapes['circle'].name:
            perimeter = float(shape['radius']) * 2 * math.pi
        else:
            perimeter = \
                distance_between_point(float(coordinates[0]['x']), float(coordinates[0]['y']),
                                       float(coordinates[1]['x']), float(coordinates[2]['y'])) \
                + distance_between_point(float(coordinates[1]['x']), float(coordinates[1]['y']),
                                         float(coordinates[2]['x']), float(coordinates[2]['y'])) \
                + distance_between_point(float(coordinates[0]['x']), float(coordinates[0]['y']),
                                         float(coordinates[2]['x']), float(coordinates[2]['y']))
    return perimeter


def main():
    if sys.argv.__len__() < 2:
        print('Неверный формат вызова, ожидается путь к файлу JSON с входными данными')
        exit(-1)
    colors = Enum('Colors', [('red', 'красный'), ('orange', 'оранжевый'), ('yellow', 'желтый'),
                             ('green', 'зеленый'), ('blue', 'голубой'), ('indigo', 'синий'), ('purple', 'фиолетовый')
                             ])
    shapes = Enum('Shapes', [('rectangle', 'прямоугольник'), ('circle', 'круг'), ('triangle', 'треугольник')])
    in_file_name = sys.argv[1]
    figures = []
    data = parse_file(in_file_name)
    for shape in data:
        i_node = {
            'name': shapes[shape['type']].value,
            'color': colors[shape['color']].value,
            'coordinates_raw': shape['coordinate'],
            'type': shape['type'],
            'coordinates': ""
        }

        i_coordinates = shape['coordinate']
        if shape['type'] == shapes['rectangle'].name:
            i_node['coordinates'] = f"левый верхний угол: {format_coordinates(i_coordinates[0])}, " \
                                    f"правый нижний угол: {format_coordinates(i_coordinates[1])}"
        else:
            if shape['type'] == shapes['circle'].name:
                i_node['coordinates'] = f"центр {format_coordinates(i_coordinates[0])}, R{shape['radius']}"
                i_node['radius'] = shape['radius']
            else:
                for coordinate in i_coordinates:
                    i_node['coordinates'] += f"{format_coordinates(coordinate)},"
                i_node['coordinates'] = i_node['coordinates'][:-1]
        i_node['perimeter'] = calculate_perimeter(shapes, i_node)
        figures.append(i_node)
    figures.sort(key=lambda item: item['perimeter'])
    for i in range(len(figures)):
        i_shape = figures[i]
        print(f"{i+1}: {i_shape['color']} {i_shape['name']}, координаты: {i_shape['coordinates']}")
        print(f"{i+1}: Периметр: {calculate_perimeter(shapes,i_shape)}")
    print(f"Всего фигур: {figures.__len__()}")


if __name__ == "__main__":
    main()
