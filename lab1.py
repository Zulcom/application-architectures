# Запуск программы осуществляется из командной строки,
#  в которой указываются: имя запускаемой программы; имя файла с исходными данными; имя файла с выходными данными.
#
# Для каждого программного объекта, загружаемого в контейнер, исходный файл должен содержать, признак,
# а также список необходимых параметров.
# Этот список должен быть представлен в формате, удобном для обработки компьютером.
#
# Функция, формирующая выходной файл, должна выдавать в него программные объекты и их текущие параметры.
# Помимо этого необходимо вывести информацию об общем количестве объектов,
# содержащихся в контейнере. Информация должна быть представлена в форме, удобной для восприятия пользователем.

# Вариант 1, задача 1, список динамический.
# Обобщенный артефакт, используемый в задании
# Плоская геометрическая фигура.

# Базовые альтернативы (уникальные параметры, задающие отличительные признаки альтернатив)
# Круг (целочисленные координата центра окружности, радиус)
# Прямоугольник (целочисленные координаты левого верхнего и правого нижнего углов)

# Параметры, общие для всех альтернатив
# Цвет фигуры (перечислимый тип) = {красный, оранжевый, желтый, зеленый, голубой, синий, фиолетовый}

# Организация контейнера
# Cписок динамический

import sys
import json
from enum import Enum


def parse_file(filename):
    try:
        with open(filename) as f:
            data = json.load(f)
        return data
    except IOError:
        print(f"Невозможно открыть файл {filename}")
        exit(-1)
    except ValueError:
        print(f"Формат данных в файле {filename} не является валидным JSON.")
        exit(-1)


def main():
    if sys.argv.__len__() < 2:
        print('Неверный формат вызова, ожидается путь к файлу JSON с входными данными')
        exit(-1)
    colors = Enum('Colors', [('red', 'красный'), ('orange', 'оранжевый'), ('yellow', 'желтый'),
                             ('green', 'зеленый'), ('blue', 'голубой'), ('indigo', 'синий'), ('purple', 'фиолетовый')
                             ])
    shapes = Enum('Shapes', [('rectangle', 'прямоугольник'), ('circle', 'круг')])
    in_file_name = sys.argv[1]
    figures = []
    data = parse_file(in_file_name)
    for shape in data:
        i_node = {
            'name': shapes[shape['type']].value,
            'color': colors[shape['color']].value,
        }
        i_coordinates = shape['coordinate']
        if shape['type'] == shapes['rectangle'].name:
            i_node['coordinates'] = f"левый верхний угол: ({i_coordinates[0]['x']},{i_coordinates[0]['y']}), " \
                                    f"правый нижний угол: ({i_coordinates[1]['x']},{i_coordinates[1]['y']})"
        else:
            i_node['coordinates'] = f"центр ({i_coordinates[0]['x']},{i_coordinates[0]['y']}), R{shape['radius']}"
        figures.append(i_node)

    for i in range(len(figures)):
        i_shape = figures[i]
        print(f"{i+1}: {i_shape['color']} {i_shape['name']}, координаты: {i_shape['coordinates']}")
    print(f"Всего фигур: {figures.__len__()}")


if __name__ == "__main__":
    main()
