# Добавить в программу, разработанную на практическом занятии №1, дополнительные программные объекты,
# расширяющие номенклатуру обрабатываемых данных.
# Добавление осуществлять в соответствии с вариантами, выданными для предыдущего задания.
# Вариант: Треугольник (три точки, задающие целочисленные координаты вершин)

import sys
import json
from enum import Enum


def parse_file(filename):
    try:
        with open(filename) as f:
            data = json.load(f)
        return data
    except IOError:
        print(f"Невозможно открыть файл {filename}")
        exit(-1)
    except ValueError:
        print(f"Формат данных в файле {filename} не является валидным JSON.")
        exit(-1)


def format_coordinates(coordinates):
    return f"({coordinates['x']},{coordinates['y']})"


def main():
    if sys.argv.__len__() < 2:
        print('Неверный формат вызова, ожидается путь к файлу JSON с входными данными')
        exit(-1)
    colors = Enum('Colors', [('red', 'красный'), ('orange', 'оранжевый'), ('yellow', 'желтый'),
                             ('green', 'зеленый'), ('blue', 'голубой'), ('indigo', 'синий'), ('purple', 'фиолетовый')
                             ])
    shapes = Enum('Shapes', [('rectangle', 'прямоугольник'), ('circle', 'круг'), ('triangle', 'треугольник')])
    in_file_name = sys.argv[1]
    figures = []
    data = parse_file(in_file_name)
    for shape in data:
        i_node = {
            'name': shapes[shape['type']].value,
            'color': colors[shape['color']].value,
        }
        i_coordinates = shape['coordinate']
        i_node['coordinates'] = ""
        if shape['type'] == shapes['rectangle'].name:
            i_node['coordinates'] = f"левый верхний угол: {format_coordinates(i_coordinates[0])}, " \
                                    f"правый нижний угол: {format_coordinates(i_coordinates[1])}"
        else:
            if shape['type'] == shapes['circle'].name:
                i_node['coordinates'] = f"центр {format_coordinates(i_coordinates[0])}, R{shape['radius']}"
            else:
                for coordinate in i_coordinates:
                    i_node['coordinates'] += f"{format_coordinates(coordinate)},"
                i_node['coordinates'] = i_node['coordinates'][:-1]
        figures.append(i_node)

    for i in range(len(figures)):
        i_shape = figures[i]
        print(f"{i+1}: {i_shape['color']} {i_shape['name']}, координаты: {i_shape['coordinates']}")
    print(f"Всего фигур: {figures.__len__()}")


if __name__ == "__main__":
    main()
