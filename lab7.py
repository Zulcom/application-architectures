#  Добавить в программу, разработанную на практическом занятии №1,
#  дополнительную процедуру, реализующую мультиметод с двумя аргументами.
#  Мультиметод должен выводить в файл информацию о своей работе и типе объектов используемых
#  в подставленных комбинациях, а также печатать содержимое принятых значений.

import json
import sys
from enum import Enum


def parse_file(filename):
    try:
        with open(filename) as f:
            data = json.load(f)
        return data
    except IOError:
        print(f"Невозможно открыть файл {filename}")
        exit(-1)
    except ValueError:
        print(f"Формат данных в файле {filename} не является валидным JSON.")
        exit(-1)


def multi_method(l_figure, r_figure):
    print(l_figure['name'] + ' - ' + r_figure['name'])


def call_multi_method(figures):
    for i in range(len(figures)):
        for j in range(len(figures) - 1, -1, -1):
            multi_method(figures[i], figures[j])


def main():
    if sys.argv.__len__() < 2:
        print('Неверный формат вызова, ожидается путь к файлу JSON с входными данными')
        exit(-1)
    colors = Enum('Colors', [('red', 'красный'), ('orange', 'оранжевый'), ('yellow', 'желтый'),
                             ('green', 'зеленый'), ('blue', 'голубой'), ('indigo', 'синий'), ('purple', 'фиолетовый')
                             ])
    shapes = Enum('Shapes', [('rectangle', 'прямоугольник'), ('circle', 'круг')])
    in_file_name = sys.argv[1]
    figures = []
    data = parse_file(in_file_name)
    for shape in data:
        i_node = {
            'name': shapes[shape['type']].value,
            'color': colors[shape['color']].value,
        }
        i_coordinates = shape['coordinate']
        if shape['type'] == shapes['rectangle'].name:
            i_node['coordinates'] = f"левый верхний угол: ({i_coordinates[0]['x']},{i_coordinates[0]['y']}), " \
                                    f"правый нижний угол: ({i_coordinates[1]['x']},{i_coordinates[1]['y']})"
        else:
            i_node['coordinates'] = f"центр ({i_coordinates[0]['x']},{i_coordinates[0]['y']}), R{shape['radius']}"
        figures.append(i_node)

    for i in range(len(figures)):
        i_shape = figures[i]
        print(f"{i+1}: {i_shape['color']} {i_shape['name']}, координаты: {i_shape['coordinates']}")
    print(f"Всего фигур: {figures.__len__()}")
    call_multi_method(figures)


if __name__ == "__main__":
    main()
